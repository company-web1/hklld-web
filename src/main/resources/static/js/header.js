$(function () {
    // 图片加载效果
    $('.slide.lazyload').addClass('loaded');
    $('img.lazyload').addClass('loaded');
    $(window).on('scroll', animate);
    animate();

    // 切换语言
    $('.header__bar--lang .lang, .header__bar--lang .icon-lang').click(function () {
        $('.header__bar--lang .selector-i').stop().slideToggle();
    });
    $('.header__bar--lang .selector-i .option').click(function () {
        var clang = $(this).attr('data-lang');
        $.ajax({
            type: 'post',
            url: '/lang',
            data: {lang: clang},
            success: function (data) {
                if (data.success) {
                    $('body').removeClass('zh').removeClass('en').addClass(clang);
                    if (clang == 'zh') {
                        $('.lang-t')[0].innerText = 'cn';
                        $('input[name=search-g]').attr('placeholder', '搜索GOA');
                    } else {
                        $('.lang-t')[0].innerText = clang;
                        $('input[name=search-g]').attr('placeholder', 'Search GOA');
                    }

                    if (typeof title !== 'undefined') {
                        document.title = title[clang];
                    }
                }
            }
        });

        $('.header__bar--lang .selector-i').slideUp();
    });

    // 设置标题
    if (typeof title !== 'undefined') {
        document.title = title['zh'];
    }

    $(window).on('scroll', animate);
    animate();

    $('body').on('click', '.header__bar--menu .icon-menu', function () {
        //打开menu
        $('.menu').addClass('is-active');
        $(this).removeClass('icon-menu').addClass('icon-close');
        $('body').css('overflow', 'hidden');

        $('.topbar').removeClass('wht');

        $('.header__bar--lang').show();

    });

    $('body').on('click', '.header__bar--menu .icon-close', function () {
        //打开menu
        $('.menu').removeClass('is-active');
        $(this).removeClass('icon-close').addClass('icon-menu');
        $('body').css('overflow', 'auto');

        $('.topbar').addClass('wht');


        $('.header__bar--lang').hide();

    });

    // 打开二级菜单
    $('.menu-f > a span').hover(function () {
        $('.menu-f > a').removeClass('active');
        $(this).parents('a').addClass('active');
        var tg_ss = $(this).parents('a').attr('data-sub');
        if (tg_ss !== undefined) {
            $('.menu-ss').hide();
            $('#' + tg_ss).show();
        } else {
            $('.menu-ss').hide();
        }
    });

    $('.menu-fb > span').click(function () {
        if ($(this).parents('.menu-fb').hasClass('active')) {
            $(this).parents('.menu-fb').removeClass('active');
            $('.menu-fb .menu-ss').slideUp();
        } else {
            $('.menu-fb').removeClass('active');
            $('.menu-fb .menu-ss').slideUp();
            $(this).parents('.menu-fb').addClass('active');
            $(this).parents('.menu-fb').find('.menu-ss').slideDown();
        }

    });
    $('body').on('click', '.header__bar--search .icon-tt.icon-close', function () {
        $('.header__bar--search').removeClass('is-active');
        $('.serhres').removeClass('is-active');
        $(this).addClass('icon-search').removeClass('icon-close');
        $('body').css('overflow', 'auto');

        $('.topbar').addClass('wht');

    });

    // 微信二维码展开
    $('.qr-opener').click(function () {
        $('.qr-shadow').addClass('open');
    });

    // 微信二维码关闭
    $('.qr-shadow .close').click(function () {
        $('.qr-shadow').addClass('close');
        setTimeout(function () {
            $('.qr-shadow').removeClass('close').removeClass('open');
        }, 400)
    });

    $('body').on('click', '.search-types .item', function () {
        if ($(this).hasClass('active')) {
            return false;
        }

        $('.search-types .item').removeClass('active');
        $('.bres').hide();

        $(this).addClass('active');
        var tg = $(this).attr('data-sblock');
        $('#' + tg).fadeIn('fast');
    });


});

function animate() {
    $('.flash-move').each(function () {
        var win = $(window);
        var winScrollTop = win.scrollTop();
        var othis = $(this);
        var oTop = parseInt(othis.offset().top);

        if (oTop <= parseInt(win.height() + winScrollTop)) {
            othis.addClass('move-on');
        }
    });
}