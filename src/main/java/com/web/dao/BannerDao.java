package com.web.dao;

import com.web.entity.BannerDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface BannerDao {
    @Select("select * from t_banner where menuId=#{menuId} and is_del=0")
    public List<Map<String,Object>> getBannerByMenuId(@Param("menuId") Long menuId);
    @Select("select * from t_banner where menuId=#{menuId} and parentId=#{parentId} and is_del=0 order by sort asc")
    public List<Map<String,Object>> getBannerByMenuIdAndParentId(@Param("menuId") Long menuId,@Param("parentId") Long parentId);
    @Select("select * from t_banner")
    public List<Map<String,Object>> getBannerAll();
    @Select("select * from t_banner where menuId=#{menuId} and parentId=#{parentId} and is_del=0 order by sort asc limit 1")
    public Map<String,Object> getBannerByParentIdAndMenuIdLimit1(@Param("menuId")Long menuId, @Param("parentId") Long parentId);
}
