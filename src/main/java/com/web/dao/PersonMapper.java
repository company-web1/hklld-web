package com.web.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface PersonMapper {

    @Select("select * from t_person order by sort asc")
    public List<Map<String,Object>> queryAll();

    @Select("select * from t_person where id=#{id}")
    public Map<String,Object> queryById(Integer id);
    @Select("select id from t_person order by sort asc")
    public List<Integer> queryAllIdBySort();
}
