package com.web.dao;

import com.web.entity.ProductDTO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface ProjectDao {
    @Select("select * from t_product where parent_Id=#{parentId} and is_del=0 order by sort")
    public List<Map<String,Object>> queryByParentId(@Param("parentId") String parentId);
    @Select("select * from t_product where  is_del=0 and id=#{id}")
    public Map<String,Object> queryDetailsByProductId(@Param("id") Long id );
    @Select("select * from t_product where parent_Id BETWEEN 57 and 62 and is_del=0 order by sort")
    public List<Map<String,Object>> queryAll();
    /**
     * 首页四个项目
     * @return
     */
    @Select("select p.id,p.parent_id,p.`name`,b.banner_url from t_product p left join t_banner b on p.id=b.parentId and p.parent_id=b.menuId " +
            "where p.is_del=0 and p.parent_id BETWEEN 57 and 62 order by b.sort asc limit 4")
    public List<Map<String,Object>> queryProductFirstPage();
    @Select("select * from t_product where parent_Id=#{parentId} and is_del=0 order by sort limit 1")
    public Map<String,Object> queryByParentIdLimit1(@Param("parentId") Long parentId);

    /**
     * 更多 查询四个项目
     */
    @Select("select * from t_product where id!=#{id} and is_del=0  and parent_id BETWEEN 57 and 62 limit 6")
    public List<Map<String,Object>> queryFourOtherProject(@Param("id") Long id);
}
