package com.web.dao;

import com.web.entity.NewsDTO;
import com.web.entity.WebInfoDTO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface CommonDao {
    @Select("select * from t_web_info")
    public Map<String,Object> getWebInfo();
    @Select("select * from t_news  where  is_del=0 order by create_time desc limit 3")
    public List<Map<String,Object>> getNewsFirst();
    @Select("select name from sys_menu where id=#{id} and is_del=0")
    public String getMenuNameById(@Param("id")Long id);

    @Select("select * from t_news  where  is_del=0 order by create_time desc")
    public List<Map<String,Object>> getNewsAll();
    @Select("select * from t_news where id=#{id} and is_del=0")
    public Map<String,Object> getNewsById(@Param("id")Long id);

    @Select("select * from t_news  where  is_del=0 and id!=#{id} order by create_time desc limit 6")
    public List<Map<String,Object>> getMoreNews(@Param("id")Long id);
    @Select("select * from t_recruit where is_del=0 order by publish_time desc")
    public List<Map<String,Object>> getJobs();
    @Insert("insert into t_message (name,tel,email,content,ip,is_del,create_time,update_time) " +
            "values (#{name},#{tel},#{email},#{content},#{ip},#{is_del},#{create_time},#{update_time})")
    public int insertMsg(Map<String,Object> map);

}
