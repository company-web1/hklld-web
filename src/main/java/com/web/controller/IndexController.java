package com.web.controller;

import com.web.dao.BannerDao;
import com.web.dao.CommonDao;
import com.web.dao.ProjectDao;
import com.web.entity.BannerDTO;
import com.web.entity.NewsDTO;
import com.web.entity.ProductDTO;
import com.web.entity.WebInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class IndexController {
    @Autowired
    private BannerDao bannerDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private CommonDao commonDao;
    @RequestMapping("/")
    public String index(Model model){
        //基本信息
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // 首页banner图
        List<Map<String,Object>> bannerPcList =  bannerDao.getBannerByMenuId(42L);
        List<Map<String,Object>> bannerMobileList =  bannerDao.getBannerByMenuId(68L);
        // 首页项目和图
        List<Map<String,Object>> projectList = projectDao.queryProductFirstPage();
        // 首页新闻和图 3个
        List<Map<String,Object>> newList = commonDao.getNewsFirst();
        model.addAttribute("webInfoDTO",webInfoDTO);
        model.addAttribute("bannerPcList",bannerPcList);
        model.addAttribute("bannerMobileList",bannerMobileList);
        model.addAttribute("projectList",projectList);
        model.addAttribute("newList",newList);
        model.addAttribute("pcSize",bannerPcList.size());
        if(bannerPcList.size()>1){
            model.addAttribute("firstPc",bannerPcList.get(0));
            model.addAttribute("lastPc",bannerPcList.get(bannerPcList.size()-1));
        }
        model.addAttribute("moSize",bannerMobileList.size());
        if(bannerMobileList.size()>1){
            model.addAttribute("firstMo",bannerMobileList.get(0));
            model.addAttribute("lastMo",bannerMobileList.get(bannerPcList.size()-1));
        }
        return "/index";
    }
}
