package com.web.controller;

import com.web.dao.BannerDao;
import com.web.dao.CommonDao;
import com.web.dao.ProjectDao;
import com.web.entity.BannerDTO;
import com.web.entity.ProductDTO;
import com.web.entity.WebInfoDTO;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class ProjectController {
    @Autowired
    private BannerDao bannerDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private CommonDao commonDao;
    @RequestMapping("/project")
    public String project(Model model){
        //查询所有项目类型，并查询排序第一的项目的首张图片
        //基本信息
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // 分类别项目信息
        Long [] arr = {57L,59L,60L,61L,62L,58L};
        List<Map<String,Object>> mapList = new ArrayList<>();
        for(int i=0;i<arr.length;i++){
            Map<String,Object> map = new HashMap<>();
            //
            String menuName = commonDao.getMenuNameById(arr[i]);
            map.put("projectType",menuName); // 项目类型名称
            map.put("projectId",arr[i]);
            // 查询项目信息
            Map<String,Object> productDTO = projectDao.queryByParentIdLimit1(arr[i]);
            if(productDTO!=null){
                // 遍历项目信息，查询图片
                Map<String,Object> bannerDTO = bannerDao.getBannerByParentIdAndMenuIdLimit1(arr[i],Long.parseLong(productDTO.get("id").toString()));
                if(bannerDTO!=null) map.put("imgUrl",bannerDTO.get("banner_url"));
            }
            mapList.add(map);
        }
        model.addAttribute("mapList",mapList);
        model.addAttribute("webInfoDTO",webInfoDTO);
        return "project";
    }
    @RequestMapping("/items/{parentId}")
    public String items(@PathVariable("parentId") String parentId, Model model){
        List<Map<String,Object>> mapList = new ArrayList<>();
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        String menuName = "";
        List<Map<String,Object>> list = null;
        if("999".equals(parentId)){ // 查询所有
            list = projectDao.queryAll();
            menuName = "项目总览";
        }else{
            // 查询所有项目信息
            list = projectDao.queryByParentId(parentId);
            menuName = commonDao.getMenuNameById(Long.parseLong(parentId));
        }

        int count=0;
        String firstImg="";
        // 查询项目首图
        for (Map<String,Object> productDTO:list) {
            Map<String,Object> map = new HashMap<>();
            map.put("itemName",productDTO.get("name"));//项目名
            map.put("itemId",productDTO.get("id"));// 项目id
            Map<String,Object> bannerDTO = bannerDao.getBannerByParentIdAndMenuIdLimit1(Long.parseLong(parentId),Long.parseLong(productDTO.get("id").toString()));
           if(bannerDTO!=null){
               map.put("imageUrl",bannerDTO.get("banner_url")); // 项目图片
               if (count==0) {
                   firstImg = bannerDTO.get("banner_url").toString();
               }
           }
           count++;
           mapList.add(map);
        }
        model.addAttribute("menuName",menuName);
        model.addAttribute("mapList",mapList);
        model.addAttribute("firstImg",firstImg);
        model.addAttribute("webInfoDTO",webInfoDTO);
        return "items";
    }
  /*  @RequestMapping("/itemsDetails")
    public String itemsDetails(){
        return "itemDetails";
    }*/
    @RequestMapping("/itemsDetails/{productId}")
    public String itemsDetails(@PathVariable("productId") String productId,Model model){

        //基本信息
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // 根据id 查询详情,
        Map<String,Object> productDTO = projectDao.queryDetailsByProductId(Long.parseLong(productId));
        List<Map<String,Object>> bannerDTOList = bannerDao.getBannerByMenuIdAndParentId(Long.parseLong(productDTO.get("parent_id").toString()),Long.parseLong(productId));
        String menuName = commonDao.getMenuNameById(Long.parseLong(productDTO.get("parent_id").toString())); // 菜单名称
        // 查询相近的项目
        List<Map<String,Object>> fourProducts = projectDao.queryFourOtherProject(Long.parseLong(productDTO.get("id").toString()));
        for(Map<String,Object> productDTO1:fourProducts){
            Map<String,Object> bannerDTO = bannerDao.getBannerByParentIdAndMenuIdLimit1(Long.parseLong(productDTO1.get("parent_id").toString()),Long.parseLong(productDTO1.get("id").toString()));
            if(bannerDTO != null) productDTO1.put("banner_url",bannerDTO.get("banner_url"));
        }
        model.addAttribute("webInfoDTO",webInfoDTO);
        model.addAttribute("productDTO",productDTO);
        model.addAttribute("bannerDTOList",bannerDTOList);
        model.addAttribute("menuName",menuName);
        model.addAttribute("fourProducts",fourProducts);
        return "itemDetails";
    }
}
