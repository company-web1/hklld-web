package com.web.controller;

import com.web.dao.CommonDao;
import com.web.dao.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class AboutController {
    @Autowired
    PersonMapper personMapper;
    @Autowired
    private CommonDao commonDao;
    @RequestMapping("/about")
    public String about(){
        // banner 图片，中间小图片

        return "about";
    }
    @RequestMapping("/coreTeam")
    public String coreTeam(Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // banner 图片，中间小图片
        List<Map<String,Object>> coreList = personMapper.queryAll();
        model.addAttribute("webInfoDTO",webInfoDTO);
        model.addAttribute("coreList",coreList);
        return "coreTeam";
    }
    @RequestMapping("/person/{id}")
    public String person(@PathVariable("id")Integer id,Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // banner 图片，中间小图片
        List<Integer> idList = personMapper.queryAllIdBySort();
        int index = idList.indexOf(id);
        Map<String,Object> person = personMapper.queryById(id);
        model.addAttribute("person",person);
        if(index!=0){
            Map<String,Object> Lperson = personMapper.queryById(idList.get(index-1));
            model.addAttribute("Lperson",Lperson);
        }
        if(index!=idList.size()-1){
            Map<String,Object> Rperson = personMapper.queryById(idList.get(index+1));
            model.addAttribute("Rperson",Rperson);
        }
        model.addAttribute("webInfoDTO",webInfoDTO);
        return "person";
    }
}
