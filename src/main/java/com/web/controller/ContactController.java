package com.web.controller;

import com.web.dao.BannerDao;
import com.web.dao.CommonDao;
import com.web.dao.ProjectDao;
import freemarker.template.utility.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class ContactController {
    @Autowired
    private BannerDao bannerDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private CommonDao commonDao;
    @RequestMapping("/contact")
    public String contact(Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        model.addAttribute("webInfoDTO",webInfoDTO);
        return "contact";
    }
    @RequestMapping("/join")
    public String join(Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        List<Map<String,Object>> res = projectDao.queryByParentId("72");
        for (Map<String,Object> r:res){
           Long parentId =  Long.parseLong(r.get("id").toString());
           List<Map<String,Object>> bannerList = bannerDao.getBannerByMenuIdAndParentId(72L,parentId);
           r.put("banners",bannerList);
        }
        //
        List<Map<String,Object>> jobs = commonDao.getJobs();
       /* for (Map<String,Object> r:res) {
            r.put("publish_time",r.get("publish_time").toString().substring(0,10));
        }*/
        model.addAttribute("jobs",jobs);
        model.addAttribute("webInfoDTO",webInfoDTO);
        model.addAttribute("res",res);
        return "join";
    }
    @RequestMapping("/message")
    public String nessage(HttpServletRequest request){
        String name = request.getParameter("name");
        String tel = request.getParameter("tel");
        String email = request.getParameter("email");
        String context = request.getParameter("context");
        String ip = request.getRemoteHost();
        Map<String,Object> map = new HashMap<>();
        map.put("name",name);
        map.put("tel",tel);
        map.put("email",email);
        map.put("content",context);
        map.put("ip",ip);
        map.put("is_del",0);
        map.put("create_time", new SimpleDateFormat("yyyy-MM-dd HH:ss:MM").format(new Date()));
        map.put("update_time", new SimpleDateFormat("yyyy-MM-dd HH:ss:MM").format(new Date()));
        commonDao.insertMsg(map);
        return "redirect:contact";
    }

    public static void main(String[] args) {
        System.out.println();
    }
}
