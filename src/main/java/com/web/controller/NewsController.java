package com.web.controller;

import com.web.dao.CommonDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Controller
public class NewsController {
    @Autowired
    CommonDao commonDao;

    @RequestMapping("/news")
    public String news(Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        // 最新的
        List<Map<String,Object>> newsList =  commonDao.getNewsAll();
        model.addAttribute("newsList",newsList);
        if(newsList!=null && newsList.size()>0){
            model.addAttribute("firstNews",newsList.get(0));
        }
        model.addAttribute("webInfoDTO",webInfoDTO);
        return "news";
    }
    @RequestMapping("/newsDetails/{id}")
    public String newsDetails(@PathVariable("id") Long id,Model model){
        Map<String,Object> webInfoDTO =  commonDao.getWebInfo(); // 查询首页文字信息等信息
        Map<String,Object> news = commonDao.getNewsById(id); // 新闻
        // 更多新闻
        List<Map<String,Object>> moreNews = commonDao.getMoreNews(id);
        model.addAttribute("webInfoDTO",webInfoDTO);
        model.addAttribute("news",news);
        model.addAttribute("moreNews",moreNews);
        return "newsDetails";
    }
}
