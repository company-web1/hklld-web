package com.web.entity;
/**
 * banner
 * @author admin
 *
 */
public class BannerDTO {
    private Long id;
    private java.util.Date updateTime;
    private java.util.Date createTime;
    private String isDel;
    private String bannerUrl;
    private Integer sort;// 排序
    private String title;// 标题
    private String url; // url
    private Integer menuId;
    private Integer parentId;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public BannerDTO() {
        super();
    }
    public BannerDTO(Long id,java.util.Date updateTime,java.util.Date createTime,String isDel,String bannerUrl,Integer sort,String title,String url,Integer munuId,Integer parentId,String type) {
        super();
        this.id = id;
        this.updateTime = updateTime;
        this.createTime = createTime;
        this.isDel = isDel;
        this.bannerUrl = bannerUrl;
        this.sort = sort;
        this.title = title;
        this.url = url;
        this.menuId = munuId;
        this.parentId = parentId;
        this.type = type;
    }
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public java.util.Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }

    public java.util.Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }

    public String getIsDel() {
        return this.isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

    public String getBannerUrl() {
        return this.bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Integer getSort() {
        return this.sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
