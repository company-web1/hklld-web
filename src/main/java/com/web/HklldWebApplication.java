package com.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("com.web.dao")
@EnableSwagger2
public class HklldWebApplication {


    public static void main(String[] args) {
        SpringApplication.run(HklldWebApplication.class,args);
    }
}
